This Java TicTacToe program was developed as a school programming project in the 2nd semester of my undergrad 
studies at Hochschule Mittweida. It utilizes the minimax algorithm and optimizes it using Alpha-Beta-pruning
to simulate an unbeatable oponent on 3x3 and 4x4 boards. The provided pdf document (German) explains the 
implementation in more detail. If you'd like to have a go at the game feel free to download the provided jar file.